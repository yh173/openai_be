from flask import Blueprint, request
import openai
import datetime
import os
from dotenv import load_dotenv
from utils.db import mysql_pool
from utils.logger_config import logger

# 加载 .env 文件
load_dotenv()

bp = Blueprint('chat', __name__)

openai.api_key = os.getenv("OPENAI_API_KEY")


@bp.route('/', methods=["POST"])
def ask_openai():
    data = request.get_json()
    prompt = data.get("prompt")

    completion = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "我在日本东京神田，我喜欢吃咖喱，牛肉饭。不喜欢韩国料理。"},
            {"role": "user", "content": prompt}
        ]
    )
    message = completion.choices[0].message.content
    # 获取当前时间
    now = datetime.datetime.now()
    # 将当前时间转换为指定格式
    now_str = now.strftime("%Y-%m-%d %H:%M:%S")
    # 插入数据
    try:
        conn = mysql_pool.get_connection()
        cursor = conn.cursor()
        sql = "INSERT INTO chatgpt_history (INPUT_CONTENT, OUTPUT_CONTENT,CREATED) VALUES (%s, %s, %s)"
        val = (prompt, message, now_str)
        cursor.execute(sql, val)
        # 处理返回结果
        result = cursor.fetchall()
        logger.debug("success:", result)
        conn.commit()
    except Exception as e:
        # 处理异常
        logger.error("DB error: ", e)
    finally:
        # 关闭游标和连接
        cursor.close()
        conn.close()
    return message

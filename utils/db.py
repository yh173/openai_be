from mysql.connector.pooling import MySQLConnectionPool
import os
from dotenv import load_dotenv

# 加载 .env 文件
load_dotenv()

# conn，创建连接
mysql_pool = MySQLConnectionPool(
    pool_size=10,  # 连接池大小
    pool_reset_session=True,  # 每次从连接池中获取连接时重置会话
    host=os.getenv("HOST"),
    port=os.getenv("PORT"),
    user=os.getenv("DB_USER"),
    password=os.getenv("DB_PASSWORD"),
    charset="utf8mb4",
    database=os.getenv("DATABASE"),
)

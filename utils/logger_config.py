import logging

# 创建 Flask 的日志记录器
logger = logging.getLogger('flask_app')
logger.setLevel(logging.DEBUG)

# 创建文件日志处理器
file_handler = logging.FileHandler('error.log')
file_handler.setLevel(logging.ERROR)  # 只记录 ERROR 级别以上的日志

# 设置日志记录器的格式
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)

# 添加日志处理器到 Flask 的日志记录器中
logger.addHandler(file_handler)

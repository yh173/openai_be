daemon = True
#debug
debug = True
# 访问地址
bind = "127.0.0.1:5050"
# 工作进程数
workers = 4
# 多线程
threads = 4
# 设置协程模式
worker_class = "gevent"
# 最大客户端并发数量，默认情况下这个值为1000。此设置将影响gevent和eventlet工作模式
worker_connections = 500
max_requests = 20
# max_requests_jitter
max_requests_jitter = 2
# 超时时间
timeout = 600
# 输出日志级别
loglevel = 'debug'
# 存放日志路径
pidfile = "log/gunicorn.pid"
# 存放日志路径
accesslog = "log/access.log"
# 存放日志路径
errorlog = "log/error.log"

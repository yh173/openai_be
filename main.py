from flask import Flask
from controllers.chat import bp as chat_bp
from flask_cors import CORS

app = Flask(__name__)
CORS(app, supports_credentials=True)

app.register_blueprint(chat_bp, url_prefix='/chat')
@app.route("/")
def haloWord():
    return "<h1>HELLO PYTHON</h1>"

if __name__ == '__main__':
    app.run(port=5051)
